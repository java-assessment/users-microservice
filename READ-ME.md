# Users Management Microservice


## Clone the repositories in this group (to the [GitHub Pages](https://gitlab.com/java-assessment))

```
1. Run the service discovery.
2. Then run the users microservice last

That way the users microservice instance can be registered on eureka
```

## Dependencies

```
$ Build project using Spring boot 2.5.5 
$ Build project using marven 3.8.2
```

## Running

```
$ Create a mysql (version 5.7) database named users_db before running the application
$ Run the main method UserManagerApplication
```

