package zw.co.celfin.usermanager;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import zw.co.celfin.usermanager.model.User;
import zw.co.celfin.usermanager.persistence.UserRepository;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataLoadRunner implements CommandLineRunner {
  private final UserRepository userRepository;

    public DataLoadRunner(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        List<User> users = new ArrayList<>();
        Collections.addAll(users
                , new User("Mr", "Chris", "Brown", LocalDate.of(2000, Month.APRIL, 15), "Musician")
                , new User("Mrs", "Theresa", "Choto", LocalDate.of(1990, Month.MARCH, 5), "Doctor")
                , new User("Mrs", "Tracy", "Mvura", LocalDate.of(1985, Month.DECEMBER, 20), "Nurse")
                , new User("Mr", "George", "Tsiga", LocalDate.of(1970, Month.JANUARY, 3), "Teacher"));
        userRepository.saveAll(users);
    }
}
