package zw.co.celfin.usermanager.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.celfin.usermanager.model.User;


public interface UserRepository extends JpaRepository<User, Long> {
}
