package zw.co.celfin.usermanager.service;

import zw.co.celfin.usermanager.dto.UserDto;
import zw.co.celfin.usermanager.model.User;

import java.util.List;

public interface UserService {
    User createUser(User user);
    User updateUser(UserDto user, Long id);
    List<User> getAllUsers();
    String deleteUser(Long id);
    User get(Long id);
}
