package zw.co.celfin.usermanager.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import zw.co.celfin.usermanager.dto.UserDto;
import zw.co.celfin.usermanager.model.User;
import zw.co.celfin.usermanager.persistence.UserRepository;

import java.util.List;
import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {
    private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User createUser(User user) {
        Objects.requireNonNull(user, "User data is null");
        User userData = userRepository.save(user);

        logger.info("Saved {} ", userData);
        return userData;
    }

    @Override
    public User updateUser(UserDto user, Long id) {
        Objects.requireNonNull(user, "User data is null");
        Objects.requireNonNull(id, "Id data is null");

        return userRepository.findById(id)
                .map(usr -> {
                    usr.setDob(user.getDob());
                    usr.setFirstName(user.getFirstName());
                    usr.setJobTitle(user.getJobTitle());
                    usr.setSurname(user.getSurname());
                    usr.setTitle(user.getTitle());

                    return userRepository.save(usr);
                }).orElseGet(() -> userRepository.save(new User(user.getTitle(), user.getFirstName(), user.getSurname(), user.getDob(), user.getJobTitle())));


    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public String deleteUser(Long id) {
        User user = get(id);
        userRepository.delete(user);
        return "User deleted successfully";
    }

    @Override
    public User get(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("User not found by Id: " + id));
    }


}
